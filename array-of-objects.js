const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appt',
        isCompleted: false
    }
];

console.log(todos[1].text);

const todoJSON = JSON.stringify(todos);
console.log(todoJSON);

//for

for(let i = 0;i <= 10; i++){
    console.log(`For Loop Number: ${1}`);
}

//while
let i = 0;
while(i<10){
    console.log(`While Loop Number: ${i}`);
    i++;
}

for(let i = 0; i < todos.length; i++){
    console.log(`For Loop Number: ${i}`);
}

for(let i = 0; i < todos.length; i++){
    console.log(todos[i].text);
}

for(let todo of todos) {
    console.log(todo.id);
}

// forEach, map, filter

todos.forEach(function(todo) {
    console.log(todo.text);
});

// const todoText = todos.map(function(todo) {
//     return todo.text;
// });

//console.log(todoText);

const todoText = todos.filter(function(todo) {
    return todo.text;
});

const todoCompleted = todos.filter(function(todo) {
    return todo.isCompleted === true;
}).map(function(todo) {
    return todo.text;
})

console.log(todoText);








